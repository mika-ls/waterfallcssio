---
title: What is WaterfallCSS?
---

# What is WaterfallCSS?

WaterfallCSS is a blend of different methodologies, frameworks and naming convensions. It was born from the frustration of having different coding styles between developers and constantly rewriting the same resets and opinionated default styles for each project.

Projects at our workplace ranged from one page lead generation forms to large Vue Single Page Applications. We knew our current way of creating a different folder structure and naming convention wasn't sustainable as our team grew.

After trying a few options, we found that none of them really suited what we wanted so decided to take the best parts of each and roll our own.

In its simplest terms, WaterfallCSS is a combination of:

-   [ITCSS by Harry Roberts](https://itcss.io/)
-   [TailwindCSS by Adam Wathan](https://tailwindcss.com/docs/what-is-tailwind/)
-   BEM naming convention

'Out of the box' WaterfallCSS gives you; a structured folder directory, responsive grid system and automaticly generated utilities based on your variables. It has slotted seamlessly into our development flow and leaves us with very light CSS, faster project completion and a CSS style that allows us to transition between projects with ease.
