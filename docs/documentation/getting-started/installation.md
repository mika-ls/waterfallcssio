# Installation

We recommend cloning the repository locally and copying it over to new projects when required, this allows you to always have an up to date versions readily available.

```git
git clone https://github.com/WaterfallCSS/WaterfallCSS.git
```

You can find the repository here: [WaterfallCSS on GitHub](https://github.com/WaterfallCSS/WaterfallCSS)
