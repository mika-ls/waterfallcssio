---
title: Methodology
pageClass: frontend-class
---

# Methodology

## Folder Structure

Inspired heavily by ITCSS, we use a folder structure that forces a style similar to the inverted triangle methododolgy. This means we start by writing more generic styles such as resets, basic HTML elements and variables before writing custom components / utilities.

The idea behind this is that you should be able to construct most of your site writing minimal custom classes.

A typical WaterfallCSS folder structure is:

```
SASS/
	0-variables
	1-tools
	2-stream
	3-elements
	4-components
	5-utilities
	pool.scss
```

The folders are prefixed with a incremented number, this keeps them displaying in the order that they are compiled.

You can find out more about the purpose of each folder here: [Code Documentation](/documentation/code/)

## Increments of 8px

By using increments of 8 pixels in sizing and spacing we create a natural consistency throughout the page. Looking through the code base you will notice the use of a `size()` css function which outputs increments of 8 pixel but as a REM value.

Taking margin as an example utility class, our SASS loops generate the following classes:

:::tip
We prefix utility classes with '.u:' to make them easily distinguishable in the view, the example below has been simplified without this prefix.
:::

```scss
@for $i from 0 through 6 {
	.mt-#{$i} {
		margin-top: size($i);
	}
}
```

```css
.mt-0 {
	margin-top: 0;
}
.mt-1 {
	margin-top: 0.8rem;
}
... .mt-6 {
	margin-top: 4.8rem;
}
```

To learn more about this way of thinking, we recommend the following posts:

-   [Intro to The 8-Point Grid System](https://builttoadapt.io/intro-to-the-8-point-grid-system-d2573cde8632)
-   [8-Point Grid: Vertical Rhythm](https://builttoadapt.io/8-point-grid-vertical-rhythm-90d05ad95032)
-   [8-Point Grid: Borders And Layouts](https://builttoadapt.io/8-point-grid-borders-and-layouts-e91eb97f5091)

## Styling default HTML elements for consistency

The starting block for most WaterfallCSS projects after installation is to style up the default HTML elements to create a 'styleguide'. Working closely with a designer allows you to put constraints on the number of font sizes, colours and fonts used throughout a design.

We believe that you can build a complete website by restricting the project to the base HTML elements.

You may find it useful to download out 'kitchen sink' HTML styleguide that contains every combination and HTML element available. Using this, you can style up each element within the 'elements' folder.

## Utility based

Along with ITCSS, we loved the idea behind [Adam Wathan's TailwindCSS](https://tailwindcss.com/docs/what-is-tailwind/) project however, a utility only approach wouldn't align with our current design process.

In WaterfallCSS limited utility classes are provided and mainly used for layout, spacing and sizing. For instance; grids, padding and flexbox helpers.

By using utilities for commonly written CSS, we can avoid much of the browser inconsistencies and reduce the amount of duplicated code.
