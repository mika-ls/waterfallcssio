# Elements

There should still be no classes used in the elements directory, instead this is the place to style up available HTML element tags.

If creating a simple site designed with WaterfallCSS in mind, it's often found that styling the elements alone gives you all the required flexiblity without custom components.

We use the number prefix on elements to help keep them in a specific order.

Modify the element files and style up all required tags with either set defaults or basic styles that are present when using the tags.
