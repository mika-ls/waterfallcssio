# Components

This is the first place where classes are used and the place to create custom components. Unlike ITCSS, we stick to everything being a 'component' as opposed to objects and other naming conventions.

There is one overide of the elements files within this already which is '0_typography'. The numbered naming allows us to overide the files in elements but keep a structured order.

Inside of the typography component file there is a 'content' class used for applying to blocks of body text.

The following mixin is used within this:

```scss
@include generate-headings(false, $content-type-scale);
```

When setting up the typography variables you set a different ratio for the body content. This allows you have larger main headings and a smaller sizing ratio for body content.

---

There is no naming convention that you must stick to when creating components although BEM is our preffered one.

::: tip
Make sure to import any files you create within the relative 'collection' file in each directory.
:::
