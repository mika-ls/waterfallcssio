# Variables

This acts as the top level of the WaterfallCSS. By identifying variables early you can cut down the override of colors, fonts etc. later on.

This folder holds the info of all variables used inside the project. If you installed the sample starting point you can see the preset variable files are

-   \_breakpoints
-   \_colors
-   \_generic
-   \_shadows
-   \_transitions
-   \_typography

### \_breakpoints

Based upon the BootstrapCSS defaults, these 4 variables define all the breakpoints used throughout. By default we use max-width for the media queries but you can switch this to min-width ordering in the generic variables file.

### \_colors

By initializing the color variables it is easier to keep the website colors consistent and you do not get "fifty shades of blue" effect.

Taking a strong influence from TailwindCSS, Waterfall uses 9 variations per colour defined. We've experimented with auto generating these shades from one given colour but found after numerous projects that it's easier to have full control over each.

9 shades is a default, but you can switch out to a single value or more / less shades. Make sure that the color-loop-array at the end of the file is updated with any new values and when compiled all utilites will be updated.

### \_generic

We designed this to be used for those little variables who didn't have a home anywhere else :cry:.

### \_shadows

6 shadows are provided by default, the array within this file is used to generate the utlitiy classes and SASS function.

### \_transitions

When applying CSS transitions it was a common issue in our projects that saw multiple timing values being used. This file allows you to define your variables in one place and these are for calling anywhere but not used to generate any helper classes.

### \_typography

The typography has gone through many iterations with the goal of getting the perfect balance between customisable and auto generated.

Apart from specifying the font families you can change the scale for the auto generated headings and paragraph variants in 2 variables:

```scss
$heading-type-scale: 1.333;
$content-type-scale: 1.1;
```

By default, h1 to h6 headings and 4 paragraph classes will be created using the scales defined. You can hardcode the font sizes if preffered within the 'generate-headings' and 'generate-paragraphs' mixins.

::: tip
Before reaching for a Google or FontFace, take a look at the wide range of [web safe fonts](https://www.cssfontstack.com/) that come pre-installed on most operating systems.
:::
