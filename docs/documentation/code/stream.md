# Stream

::: tip
In a Waterfall, the stream is where the flow starts. We use this directory term as it's the first place we set code that directly touches the elements.
:::

### \_reset

A combination of multiple browser resets, this starting point removes / resets most predfined browser styles so that you start with a consistent base.

### \_defaults

An opinionated file that re-applies specific styles that we 'believe' should be the correct defaults.

This is the best place to put those styles that you often add to every element when you start a new project.
