# Utilities

One of the most powerful features of WaterfallCSS, utilty classes.

Instead of having a utility class for every style, we've carefully selected them based on most common usage and wheir purpose. The aim of utilties within WFCSS is to aid with layout, spacing and commonly applied styles.

::: tip
Missing a utility? Open a pull request / issue on GitHub.
:::

There is heavy use of a mixin across the utility files:

```scss
@include generate-colors("utility-name", "css-property");

@include generate-colors("bg", "background-color");
```

If you wish to auto generate any classes based off the defined colours in your variables, add the mixin to the correct place.

---

## \_background

| CLASS                          | PROPERTY                           |
| ------------------------------ | ---------------------------------- |
| u:fill-[colour-name]-[shade]   | fill: #hex                         |
| u:stroke-[colour-name]-[shade] | stroke: #hex                       |
| u:bg-[colour-name]-[shade]     | background-color: #hex             |
| u:bg-cover                     | background-size: cover;            |
| u:bg-contain                   | background-size: contain;          |
| u:bg-center                    | background-position: center;       |
| u:bg-top                       | background-position: top;          |
| u:bg-bottom                    | background-position: bottom;       |
| u:bg-left                      | background-position: left;         |
| u:bg-left-bottom               | background-position: left bottom;  |
| u:bg-left-top                  | background-position: left top;     |
| u:bg-right                     | background-position: right;        |
| u:bg-right-bottom              | background-position: right bottom; |
| u:bg-right-top                 | background-position: right top;    |

## \_display

| CLASS          | PROPERTY               |
| -------------- | ---------------------- |
| u:hidden       | display: none;         |
| u:block        | display: block;        |
| u:inline-block | display: inline-block; |

## \_flex

| CLASS              | PROPERTY                        |
| ------------------ | ------------------------------- |
| u:flex             | display: flex;                  |
| u:flex-wrap        | flex-wrap: wrap;                |
| u:flex-nowrap      | flex-wrap: nowrap;              |
| u:flex-col         | flex-direction: column;         |
| u:flex-col-reverse | flex-direction: column-reverse; |
| u:flex-row         | flex-direction: row;            |
| u:flex-row-reverse | flex-direction: row-reverse;    |
| u:flex-1           | flex: 1 1 0%;                   |
| u:flex-initial     | flex: 0 1 auto;                 |
| u:justify-start    | justify-content: flex-start;    |
| u:justify-center   | justify-content: center;        |
| u:justify-end      | justify-content: flex-end;      |
| u:justify-around   | justify-content: space-around;  |
| u:items-start      | align-items: start;             |
| u:items-center     | align-items: center;            |
| u:items-end        | align-items: end;               |
| u:items-baseline   | align-items: baseline;          |
| u:w-[X]/12         | width: [X]%                     |

## \_overflow

| CLASS               | PROPERTY            |
| ------------------- | ------------------- |
| u:overflow-hidden   | overflow: hidden;   |
| u:overflow-x-hidden | overflow-x: hidden; |
| u:overflow-y-hidden | overflow-y: hidden; |
| u:overflow-scroll   | overflow: scroll;   |
| u:overflow-x-scroll | overflow-x: scroll; |
| u:overflow-y-scroll | overflow-y: scroll; |

## \_shadow

| CLASS              | PROPERTY                     |
| ------------------ | ---------------------------- |
| u:shadow-[variant] | box-shadow: [variant value]; |

## \_sizing

| CLASS          | PROPERTY                                  |
| -------------- | ----------------------------------------- |
| u:w-[X]        | width: [X]px (based upon 8px scale);      |
| u:max-w-[X]    | max-width: [X]px (based upon 8px scale);  |
| u:min-w-[X]    | min-width: [X]px (based upon 8px scale);  |
| u:h-[X]        | height: [X]px (based upon 8px scale);     |
| u:max-h-[X]    | max-height: [X]px (based upon 8px scale); |
| u:min-h-[X]    | min-height: [X]px (based upon 8px scale); |
| u:h-screen     | height: 100vh;                            |
| u:h-full       | height: 100%;                             |
| u:min-h-screen | min-height: 100vh;                        |
| u:min-h-full   | min-height: 100%;                         |
| u:w-full       | width: 100%;                              |
| u:max-w-full   | max-width: 100%;                          |

## \_spacing

##### Margin

| CLASS      | PROPERTY                                                                     |
| ---------- | ---------------------------------------------------------------------------- |
| u:m-[X]    | margin: [X]px (based upon 8px scale);                                        |
| u:mx-[X]   | margin-left: [X]px; margin-right: [X]px;                                     |
| u:my-[X]   | margin-top: [X]px; margin-bottom: [X]px;                                     |
| u:m-auto   | margin: auto;                                                                |
| u:ml-auto  | margin-left: auto;                                                           |
| u:mr-auto  | margin-right: auto;                                                          |
| u:mx-auto  | margin-left: auto; margin-right: auto;                                       |
| u:my-auto  | margin-top: auto; margin-bottom: auto;                                       |
| u:my-auto  | margin-top: auto; margin-bottom: auto;                                       |
| u:m[X]-[Y] | margin-[X..]: [Y]px ('X' can be 't, r, b, l' for 'top, right, bottom, left') |

##### Padding

| CLASS      | PROPERTY                                                                      |
| ---------- | ----------------------------------------------------------------------------- |
| u:p-[X]    | padding: [X]px (based upon 8px scale);                                        |
| u:px-[X]   | padding-left: [X]px; padding-right: [X]px;                                    |
| u:py-[X]   | padding-top: [X]px; padding-bottom: [X]px;                                    |
| u:p[X]-[Y] | padding-[X..]: [Y]px ('X' can be 't, r, b, l' for 'top, right, bottom, left') |

## \_typography

| CLASS                        | PROPERTY                                                        |
| ---------------------------- | --------------------------------------------------------------- |
| u:text-[colour-name]-[shade] | color: #hex                                                     |
| u:h[X]                       | font-size: [X]px; (based upon heading variables, options 1 - 6) |
| u:p[X]                       | font-size: [X]px; (based upon paragraph variables)              |
| u:text-left                  | text-align: left;                                               |
| u:text-center                | text-align: center;                                             |
| u:text-right                 | text-align: right;                                              |
| u:text-capitalize            | text-transform: capitalize;                                     |
| u:text-uppercase             | text-transform: uppercase;                                      |
| u:text-lowercase             | text-transform: lowercase;                                      |
| u:text-underline             | text-decoration: underline;                                     |
| u:text-no-decoration         | text-decoration: none;                                          |
| u:font-serif                 | font-family: \$font-serif;                                      |
| u:font-sans-serif            | font-family: \$font-sans-serif;                                 |
| u:font-monospace             | font-family: \$font-monospace;                                  |
| u:font-hairline              | font-weight: 100;                                               |
| u:font-thin                  | font-weight: 200;                                               |
| u:font-light                 | font-weight: 300;                                               |
| u:font-normal                | font-weight: 400;                                               |
| u:font-medium                | font-weight: 500;                                               |
| u:font-semibold              | font-weight: 600;                                               |
| u:font-bold                  | font-weight: 700;                                               |
| u:font-extrabold             | font-weight: 800;                                               |
| u:font-black                 | font-weight: 900;                                               |
