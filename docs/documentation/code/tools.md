# Tools

Within tools we have have functions and mixins that can be used within your code or are used in generating classes and element styles.

### \_functions

#### size()

All spacing based utilities use the size() function to keep the 8px increments. It's also recommended to use this method if defining sizes and not using utilities in the view.

```scss
.example {
	margin: size(1); // 8px
}
```

```html
<div class="u:py-2">16px Y axis padding</div>
```

#### color() / shadow()

Instead of directly using values, reference the helper functions to use your predefined variables.

By default, WaterfallCSS comes with 92 colours and 6 shadows.

```scss
.example {
	background-color: color("red", 500);
	box-shadow: shadow("sm");
}
```

### \_mixins

4 breakpoints are defined based on your variables, these are used as the following:

```scss
.example {
	@include screen-small() {
		display: none;
	}
}
```

As with the functions, there is a few mixins that are available as 'helpers' out of the box.

#### absolute-center()

Position an element vertically and horizontally central without knowing its width or height.

```scss
@mixin absolute-center() {
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
}
```

#### dimensions()

Define width x height in one line.

```scss
@mixin dimensions($width, $height) {
	width: $width;
	height: $height;
}
```

```scss
.example {
	@include dimensions(size(3), size(8));
}
```
