module.exports = {
	title: "WaterfallCSS",
	description:
		"Scalable, structured and maintainable CSS methodology for all project sizes.",
	themeConfig: {
		lastUpdated: "Last Updated", // string | boolean
		nav: [
			{ text: "Home", link: "/" },
			{ text: "Documentation", link: "/documentation/introduction/" },
			{ text: "Twitter", link: "https://twitter.com/WaterfallCSS" }
		],
		// sidebar: "auto"
		sidebar: [
			{
				title: "Introduction",
				collapsable: false,
				children: ["/documentation/introduction/"]
			},
			{
				title: "Getting Started",
				collapsable: false,
				children: [
					"/documentation/getting-started/",
					"/documentation/getting-started/installation",
					"/documentation/getting-started/setting-up-the-project"
				]
			},
			{
				title: "Code",
				collapsable: false,
				children: [
					"/documentation/code/",
					"/documentation/code/tools",
					"/documentation/code/stream",
					"/documentation/code/elements",
					"/documentation/code/components",
					"/documentation/code/utilities"
				]
			}
			/*{
				title: "Further reading",
				collapsable: false,
				children: ["/documentation/further-reading/"]
			}*/
		]
	},

	plugins: [
		// 	[
		// 		"@vuepress/google-analytics",
		// 		{
		// 			ga: "UA-132978239-1"
		// 		}
		// 	]
	],
	// markdown: {
	// 	lineNumbers: true
	// },
	head: [["link", { rel: "icon", href: "/images/favicon.ico" }]]
};
