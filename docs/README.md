---
home: true
heroImage: /images/icon.svg
heroText: WaterfallCSS
tagline: A CSS framework / methodology inspired by ITCSS and TailwindCSS.
actionText: Get Started →
actionLink: /documentation/introduction/
#features:
#- title: Minimal Setup
#details: The utility classes are generated based upon your variables. During the build process, we create 20 (by default) variants of each variable colour which gives you #access to a large array of utilities for backgrounds, font colours and borders. Use the provided index page to see a style guide as soon as you compile the SASS.
#- title: Structured & Scalable
#details: The folder structure defines a flow (the 'waterfall') that solves alot of the issues with the natural cascading of CSS. You should never feel the need to create #overriding styles or use the !important tag again. The focus on styling elements first, reduces the custom CSS classes written which makes it easier for other developers to #get involved, maintain and scale.
#- title: Quickly Build
#details: Define your variables, style up the basic HTML elements and start building. Over many projects, we've learnt that you can build complex projects with little custom #CSS by utilising consistent styling and using utility classes.
footer: MIT Licensed | Copyright © 2019
---
